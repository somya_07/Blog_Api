class ApplicationController < ActionController::API
  #before_action :configure_permitted_parameter, if: :devise_controller?
  #before_action :authenticate_user
   include Knock::Authenticable


  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(:username, :email, :password, :password_confirmation)
    end

    devise_parameter_sanitizer.permit(:sign_in) do |user_params|
      user_params.permit(:email, :password)
    end
  end


   private

   def authenticate_v1_user
     authenticate_for V1::User
   end


end
