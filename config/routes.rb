Rails.application.routes.draw do


  resources :articles
  #, only:[:create, :show, :update, :destroy]
  post 'user_token' => 'user_token#create'
  devise_for :users
  get 'home/welcome'

end
