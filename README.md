<<<<<<< HEAD

MY Blog_Api

  This is an RESTful Api built on Rails. 

What it does-

* Authentication-1. Devise
                 2. JWT 
* Allows you to perform crud operations

It includes-

* Mysql database
* Scaffold to perform crud
* Validation of content min 5
* Faker gem to have random articles content
* Request hit through Postman
* Testing through RSpec

What to do-

* Clone the repo git@gitlab.com:somya_07/Blog_Api.git
* Go to repo directory in terminal and- 1. Run bundle install
                                        2. Run rake db:create (to create db)
                                        3. Run rake db:migrate
                                        4. Start the server through rails s/server
* Launch the Postman  
* Taadaa hit request at http://localhost:3000


>>>>>>> 98cffca12b51a66457c1a21ec5cd312fb12d14b7
